﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Profesor { 

    public class Grid : MonoBehaviour
    {
        private void Awake()
        {
            Cell.OnTurnOffConstructionMode += HandlenTurnOffConstructionMode;
        }

        void HandlenTurnOffConstructionMode()
        {
            int children = transform.childCount;
            for (int i = 0; i < children; ++i)
            {
                transform.GetChild(i).GetComponent<Cell>().MakeConstructionModeAndAttemptsFalse();
            }
        }

        private void OnDestroy()
        {
            Cell.OnTurnOffConstructionMode -= HandlenTurnOffConstructionMode;
        }
    }
}
