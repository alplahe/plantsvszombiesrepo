﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Profesor
{
    enum CellType { Empty, Peashooter, Sunflower }

    public class Cell : MonoBehaviour
    {
        [SerializeField] private bool constructionMode = false;
        [SerializeField] CellType celltype = CellType.Empty;

        [SerializeField] bool attemptToMakeAPeaShooter = false;
        [SerializeField] bool attemptToMakeASunflower = false;

        [SerializeField] private GameObject peashooterPrefab;
        [SerializeField] private GameObject sunflowerPrefab;

        public delegate void TurnOffConstructionMode();
        public static event TurnOffConstructionMode OnTurnOffConstructionMode;

        private void Awake()
        {
            PeashooterButton.OnAttemptToMakeAPeashooter += HandleOnAttemptToMakeAPeashooter;
            SunflowerButton.OnAttemptToMakeASunflower += HandleOnAttemptToMakeASunflower;
        }

        public void MakeConstructionModeAndAttemptsFalse()
        {
            constructionMode = false;
            attemptToMakeAPeaShooter = false;
            attemptToMakeASunflower = false;
        }

        private void OnMouseDown()
        {
            if (constructionMode == false)
            {
                return;
            }

            if (celltype != CellType.Empty)
            {
                return;
            }

            if (attemptToMakeAPeaShooter)
            {
                Debug.Log("wanna create peashooter");
                CreatePeashooter();
                return;
            }

            if (attemptToMakeASunflower)
            {
                Debug.Log("wanna create sunflower");
                CreateSunflower();
                return;
            }
        }

        void CreatePeashooter()
        {
            celltype = CellType.Peashooter;
            GameObject peashooterClon = Instantiate(peashooterPrefab, transform);
            peashooterClon.transform.position = gameObject.transform.position;
            Debug.Log("creating peashooter");
            if (OnTurnOffConstructionMode != null) OnTurnOffConstructionMode();
        }

        void CreateSunflower()
        {
            celltype = CellType.Sunflower;
            GameObject sunFlowerClon = Instantiate(sunflowerPrefab, transform);
            sunFlowerClon.transform.position = gameObject.transform.position;
            Debug.Log("creating sunflower");
            if (OnTurnOffConstructionMode != null) OnTurnOffConstructionMode();
        }

        void HandleOnAttemptToMakeAPeashooter()
        {
            //Debug.Log("HandleOnAttemptToMakeAPeashooter");
            constructionMode = true;
            attemptToMakeAPeaShooter = true;
        }

        void HandleOnAttemptToMakeASunflower()
        {
            //Debug.Log("HandleOnAttemptToMakeASunflower");
            constructionMode = true;
            attemptToMakeASunflower = true;
        }

        private void OnDestroy()
        {
            PeashooterButton.OnAttemptToMakeAPeashooter -= HandleOnAttemptToMakeAPeashooter;
            SunflowerButton.OnAttemptToMakeASunflower -= HandleOnAttemptToMakeASunflower;
        }
    }
}