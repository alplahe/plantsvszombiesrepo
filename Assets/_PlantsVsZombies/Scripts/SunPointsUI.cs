﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SunPointsUI : MonoBehaviour
{
    Text sunPointsText;

    // Start is called before the first frame update
    void Start()
    {
        sunPointsText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeSunPointsUIText(int points)
    {
        sunPointsText.text = points.ToString();
    }
}
