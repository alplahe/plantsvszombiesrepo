﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Profesor
{
    public class SunResources : MonoBehaviour
    {
        private int sunPoints = 500;
        // ALBERTO: me invento una restriccion por tamaño
        private const int maxSunPoints = 9999;

        [SerializeField] private GameObject sunPointsUIGO;

        private void Awake()
        {
            Sun.OnAddSunPoints += HandleOnAddSunPoints;
        }

        private void Start()
        {
            sunPointsUIGO.GetComponent<SunPointsUI>().ChangeSunPointsUIText(sunPoints);
        }

        void HandleOnAddSunPoints(int points)
        {
            sunPoints += points;

            if(sunPoints > maxSunPoints)
            {
                sunPoints = maxSunPoints;
            }

            sunPointsUIGO.GetComponent<SunPointsUI>().ChangeSunPointsUIText(sunPoints);
        }

        public bool SustractSunPoints(int sunPointsToSustract)
        {
            if(sunPointsToSustract <= sunPoints)
            {
                sunPoints -= sunPointsToSustract;
                sunPointsUIGO.GetComponent<SunPointsUI>().ChangeSunPointsUIText(sunPoints);

                return true;
            }
            return false;
        }

        void OnDestroy()
        {
            Sun.OnAddSunPoints -= HandleOnAddSunPoints;
        }
    }
}