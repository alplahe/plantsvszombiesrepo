﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Profesor
{
    public class ButtonClick : MonoBehaviour
    {
        void Start()
        {
            Debug.Log("Función start del ButtonClick");
        }

        // Función a la que se llamará cuando se cliquee en el botón
        // StartAdventure del menú principal
        public void ClickButtonStartAdventure()
        {
            Debug.Log("Se ha cliqueado en el botón StartAdventure");
        }
        public void ClickButtonMiniGames()
        {
            Debug.Log("Se ha cliqueado en el botón MiniGames");
        }
        public void ClickButtonPuzzle()
        {
            Debug.Log("Se ha cliqueado en el botón Puzzle");
        }
        public void ClickButtonSurvival()
        {
            Debug.Log("Se ha cliqueado en el botón Survival");
        }
    }
}
