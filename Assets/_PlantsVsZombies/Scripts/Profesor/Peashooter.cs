﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Profesor
{
    public class Peashooter : MonoBehaviour
    {
        // necesitamos dos variables para poder lanzar guisantes:
        // una que será el tiempo en el que lanza cada guisante,
        // y otra para comprobar el tiempo que ha pasado
        public float tiempoDisparo;
        public float tiempo;

        // las variables necesarias para disparar son dos:
        // El Objeto a disparar (guisante)
        // La posición en donde se va a colocar inicialmente
        // ese objeto (guisante)
        public GameObject peaPrefab;
        public Transform posicionInicialGuisante;

        // Necesitamos saber a qué animator tenemos que mandar
        // el trigger de "Shoot"
        public Animator miAnimator;

        // para evitar que se dispare varias veces seguidas,
        // creamos un booleano para que NO dispare mientras la
        // animación de disparo esté activa
        public bool estaDisparando;


        void Start()
        {
            estaDisparando = false;
            tiempo = 0.0f;
            // Al iniciar el código, le decimos a Unity que nos 
            // CONSIGA un COMPONENTE de tipo ANIMATOR, y lo guardas
            // en la variable "miAnimator".
            // Esto simula el arrastrar el componente "Animator" en
            // el inspector y ponerlo en la variable miAnimator de
            // este script
            miAnimator = gameObject.GetComponent<Animator>();
        }
        
        void Update()
        {
            // Si está en plena animación de disparar, entonces
            // ignoro el resto de la función
            if (estaDisparando==true)
            {
                return;
            }
            // como siempre, en la variable "tiempo" sumaremos
            // el tiempo que va pasando
            tiempo += Time.deltaTime;
            if (tiempo >= tiempoDisparo)
            {
                // si llega el momento de disparar, disparamos
                miAnimator.SetTrigger("Shoot");
                estaDisparando = true;
            }
        }

        public void Disparar()
        {
           // Debug.Log("Disparamos un guisante");
            // Primero, CLONAMOS el objeto que nos interesa: guisante
            GameObject guisanteClonado = Instantiate(peaPrefab);
            // después, lo colocamos en la posición en la que tenemos
            // establecido que se cree
            guisanteClonado.transform.position 
                = posicionInicialGuisante.position;

            tiempo = 0.0f;
            estaDisparando = false;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            //Debug.Log("El lanzaguisantes ha colisionado con "
            //    + collision.gameObject.name);

            // Una vez choquemos con algo, comprobamos si es un zombie
            // y destruimos el girasol
            Zombie zombieChocado = collision.gameObject.GetComponent<Zombie>();

            if (zombieChocado != null)
            {
                Destroy(gameObject);
            }
        }
    }
}
