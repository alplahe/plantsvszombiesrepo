﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Profesor
{

    public class Sunflower : MonoBehaviour
    {
        // creamos las variables que se van a utilizar
        public float minTiempoGeneracion;
        public float maxTiempoGeneracion;

        // necesitamos también guardar el tiempo exacto en el que vamos
        // a generar el sol, entre minTiempo y maxTiempo
        public float tiempoGeneracion;

        // finalmente, necesitamos una variable para guardar el tiempo
        // que ha pasado desde la última generación de un sol, para 
        // comprobar si debemos generar un nuevo sol
        public float tiempo;

        // Necesitamos saber qué Prefab vamos a copiar y pegar cada 
        // vez que generemos un "sun"
        public GameObject sunPrefab;

        // Necesitamos también saber qué animador va a llamar
        // al trigger "GenerateSun"
        public Animator miAnimator;

        // Necesitamos saber el tiempo durante el cual se está
        // animando el girasol para generar un sol, PERO durante
        // ese tiempo NO tiene que pasar el tiempo, ni comprobar
        // nada: simplemente ESPERA a que se genere el sol
        public bool estaGenerandoSol;


        void Start()
        {
            EstablecerTiempoGeneracion();
        }

        void Update()
        {
            // si está generando un sol (es decir, se está animando
            // para generarlo pero todavía no se ha creado),
            // entonces NO hacemos absolutamente NADA
            if (estaGenerandoSol == true)
            {
                // Sal de la función sin hacer nada más
                return;
            }

            // if (estaGenerandoSol) return;


            // en cada fotograma, vamos a aumentar el tiempo que
            // ha pasado
            tiempo += Time.deltaTime;
            // tiempo = tiempo + Time.deltaTime;            

            // si el tiempo que ha pasado es igual (o mayor) que
            // el tiempo que hemos establecido en que se genere
            // el sol, entonces GENERAMOS un sol
            if (tiempo >= tiempoGeneracion)
            {
                //Debug.Log("Generamos un nuevo sol!");
                // antes de generar el sol, le mandamos al animator
                // el trigger "GenerateSun", que hará que se inicie
                // la animación de "GenerateSun"
                miAnimator.SetTrigger("GenerateSun");
                // ponemos el FLAG de está generando sol a "true"
                // para que NO se haga nada dentro del script
                // hasta que se genere realmente el sol desde la
                // animación
                estaGenerandoSol = true;
            }
        }

        // Establece un tiempo tiempoGeneracion teniendo en cuenta
        // el maxTiempo y el minTiempo
        void EstablecerTiempoGeneracion()
        {
            tiempoGeneracion = Random
                .Range(minTiempoGeneracion, maxTiempoGeneracion);
            tiempo = 0.0f;
        }

        void GenerarSol()
        {
            // Instanciamos un nuevo sol (básicamente, CTRL-D
            // al prefab de sol)
            GameObject solClonado = Instantiate(sunPrefab);
            // Establecemos la posición del nuevo solClonado a 
            // la posición que tiene el girasol que lo ha creado
            solClonado.transform.position = gameObject.transform.position;
            EstablecerTiempoGeneracion();
            estaGenerandoSol = false;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
           // Debug.Log("El girasol ha colisionado con "
            //    + collision.gameObject.name);

            // Una vez choquemos con algo, comprobamos si es un zombie
            // y destruimos el girasol
            Zombie zombieChocado = collision.gameObject.GetComponent<Zombie>();

            if (zombieChocado != null)
            {
                Destroy(gameObject);
            }            
        }
    }
}
