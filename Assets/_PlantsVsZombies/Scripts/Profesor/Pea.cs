﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Profesor
{
  public class Pea : MonoBehaviour
  {
    // Necesitamos poder modificar la velocidad a la que
    // se mueve el guisante
    public float velocidad;

    // Decidimos desde diseño (inspector de Unity) cuánto
    // daño hace un guisante a un zombie normal
    public int dano;

    // Necesitamos saber a qué animador tenemos que mandar
    // el trigger "Explode"
    public Animator miAnimator;

    // Para evitar que el guisante se mueva cuando ya ha explotad
    // uso este booleano
    public bool estaExplotando;

    // Use this for initialization
    void Start()
    {
      estaExplotando = false;
      miAnimator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
      if (estaExplotando == true)
      {
        return;
      }
      // en cada fotograma, movemos el guisante hacia la 
      // derecha a la velocidad que consideremos
      gameObject.transform.Translate(
      Vector3.right
      * Time.deltaTime
      * velocidad, Space.World);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
      //Debug.Log("El guisante ha colisionado con "
       //   + collision.gameObject.name);
      // En este momento, hago que el guisante explote
      miAnimator.SetTrigger("Explode");
      estaExplotando = true;

      // Una vez choquemos con algo, comprobamos si es un zombie
      // y le quitamos la vida que tengamos como "dano"
      Zombie zombieChocado = collision.gameObject.GetComponent<Zombie>();
      // Si hemos chocado contra un ZOMBIE de verdad (y no el
      // killzone); la variable zombieChocado no será nula
      if (zombieChocado != null)
      {
        zombieChocado.RecibirDano(dano);
      }


    }

    public void Destruir()
    {
      Destroy(gameObject);
    }
  }
}
