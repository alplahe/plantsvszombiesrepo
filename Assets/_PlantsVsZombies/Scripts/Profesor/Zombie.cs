﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    // Necesitamos dos variables para la vida del zombie:
    // la vida máxima y la actual
    public int vidaActual;
    public int vidaMaxima;

    // Necesitamos poder modificar la velocidad a la que
    // se mueve el guisante
    public float velocidad;


    // Use this for initialization
    void Start()
    {
        // Al iniciar un zombie, ponemos la vida actual al máximo
        vidaActual = vidaMaxima;
    }

    public void RecibirDano(int dano)
    {
        vidaActual -= dano;
        // Equivalente: vidaActual = vidaActual - dano;
        if (vidaActual <=0)
        {
            Morir();
        }
    }

    public void Morir()
    {
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        // en cada fotograma, movemos el zombie hacia la 
        // izquierda a la velocidad que consideremos
        gameObject.transform.Translate(
        Vector3.left
        * Time.deltaTime
        * velocidad);
    }
}
