﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Profesor
{
    public class Sun : MonoBehaviour
    {
        public delegate void AddSunPoints(int points);
        public static event AddSunPoints OnAddSunPoints;

        // ALBERTO: se añade el tag SerializeField para mostrarlo en el inspector siendo privado
        // Podria declararlo const si no quisieramos que lo modificara el diseñador
        [SerializeField] private int points = 25;

        // Creamos las variables que sirvan para gestionar
        // el tiempo de vida del sol
        public float tiempoVidaMaximo;
        public float tiempo;

        // Necesitamos un boolean (verdadero-falso) para controlar
        // si hemos cliqueado ya en el mismo sol, para evitar
        // que se cliquee en él varias veces (y así evitar el bug
        // de sumar sol varias veces seguidas)
        public bool haCliqueadoEnSol;

        // Necesitamos saber a qué animador le vamos a enviar
        // el trigger de "Taken"
        public Animator miAnimator;

        void Start()
        {
            tiempo = 0.0f;
            haCliqueadoEnSol = false;
            // Al iniciar el código, le decimos a Unity que nos 
            // CONSIGA un COMPONENTE de tipo ANIMATOR, y lo guardas
            // en la variable "miAnimator".
            // Esto simula el arrastrar el componente "Animator" en
            // el inspector y ponerlo en la variable miAnimator de
            // este script
            miAnimator = gameObject.GetComponent<Animator>();
        }

        void Update()
        {
            // Primero, hacemos que pase el tiempo y lo guardamos
            // en la variable "tiempo"
            tiempo += Time.deltaTime;
            // Si ha pasado más tiempo del que tiene de vida máximo
            // el sol, entonces hacemos que desaparezca
            if (tiempo >= tiempoVidaMaximo)
            {
                Destruir();
            }
        }

        void Destruir()
        {
            // Destruimos el Game Object en el que está este script
            // (básicamente, es cliquear en el GameObject y pulsar
            // suprimir)
            Destroy(gameObject);
        }

        private void OnMouseDown()
        {
            if (haCliqueadoEnSol == true)
            {
                Debug.Log("Ya hemos cliqueado en el sol antes, así que ignoramos");
            }
            else
            {
                // Si ya hemos cliqueado en el sol con anterioridad,
                // no hacemos nada del resto
                //Debug.Log("Hemos cliqueado en el sol");
                // cuando cliqueemos en el sol, le mandamos al Animator
                // que se anime para que se coja el sol
                miAnimator.SetTrigger("Taken");
                haCliqueadoEnSol = true;
                // TO DO: sumar el sol a nuestra reserva de recursos

                if (OnAddSunPoints != null) OnAddSunPoints(points);
            }

        }
    }

}