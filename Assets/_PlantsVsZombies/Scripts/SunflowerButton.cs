﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Profesor
{
    public class SunflowerButton : MonoBehaviour
    {
        public delegate void AttemptToMakeASunflower();
        public static event AttemptToMakeASunflower OnAttemptToMakeASunflower;

        [SerializeField] private GameObject sunResourcesGO;
        private int sunflowerSunPointsCost = 50;

        public void AttemptToMakeASunflowerButton()
        {
            Debug.Log("AttemptToMakeASunflowerButton");

            if (sunResourcesGO.GetComponent<SunResources>().SustractSunPoints(sunflowerSunPointsCost) == false)
            {
                Debug.Log("not enough money!");
                return;
            }
            Debug.Log("enough money!");
            if (OnAttemptToMakeASunflower != null) OnAttemptToMakeASunflower();
        }
    }
}
