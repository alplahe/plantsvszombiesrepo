﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Profesor
{
    public class PeashooterButton : MonoBehaviour
    {
        public delegate void AttemptToMakeAPeashooter();
        public static event AttemptToMakeAPeashooter OnAttemptToMakeAPeashooter;

        [SerializeField] private GameObject sunResourcesGO;
        private int peashooterSunPointsCost = 100;

        public void AttemptToMakeAPeashooterButton()
        {
            Debug.Log("AttemptToMakeAPeashooterButton");

            if (sunResourcesGO.GetComponent<SunResources>().SustractSunPoints(peashooterSunPointsCost) == false)
            {
                Debug.Log("not enough money!");
                return;
            }
            Debug.Log("enough money!");
            if (OnAttemptToMakeAPeashooter != null) OnAttemptToMakeAPeashooter();
        }
    }
}
